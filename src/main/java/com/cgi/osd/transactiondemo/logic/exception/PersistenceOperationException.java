package com.cgi.osd.transactiondemo.logic.exception;

/**
 * This class is used for representing persistence exceptions that are not lock exceptions.
 *
 */
public class PersistenceOperationException extends Exception {

    private static final long serialVersionUID = 1L;

    public PersistenceOperationException(String message) {
	super(message);
    }

}
